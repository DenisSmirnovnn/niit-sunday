import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class starsymbols {

    public static void main(String[] args) {
        final String filepath = "starsymbols.txt";
        final int ch_width = 5;  // ширина символа
        final int ch_height = 7; // высота символа

        String[] ch_strings = new String[ch_height]; // прочитанные из файла строки
        String[] out_strings = new String[ch_height]; // строки для вывода на экран

        if (args.length < 1) {
            System.out.println("Parameters: [1] - string for making show!");
            return;
        }
        try (FileReader fr = new FileReader(filepath)) {
            BufferedReader br = new BufferedReader(fr);
            int i = 0;
            String line = br.readLine(); // считаем сначала первую строку
            while ((line != null) && (i < ch_height)) {
                out_strings[i] = ""; // ининициализируем строки для вывода (иначе там будет null)
                ch_strings[i++] = line;
                line = br.readLine(); // считываем остальные строки в цикле
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage()); // если файл не в папке проекта
            return;
        }

        int k;
        for (int i = 0; i < args[0].length(); i++) {
            int sym = ((int) args[0].charAt(i)) - 0x30; // возьмем каждый символ и покажем его
            if ((sym >= 0) && (sym <= 9)) { // проверим, что он от 0 до 9
                for(k=0;k<ch_height;k++) {
                    out_strings[k] += ch_strings[k].substring(sym*ch_width,(sym+1)*ch_width); // "выкусим" их исходной строки кусочек цифры
                }
            }
        }
        for(k=0;k<ch_height;k++) System.out.println(out_strings[k]); // вывод того, что получилось
    }

}
