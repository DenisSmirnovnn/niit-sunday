package SQRT;

/**
 * Created by ulizko on 27.06.2017.
 */
public class Program {
    public static void main(String[] args)
    {
        double val=Double.parseDouble(args[0]);
        double pre=Double.parseDouble(args[1]);
        Sqrt sqrt=new Sqrt(val, pre);
        double result=sqrt.calc();
        System.out.println("Sqrt of "+val+"="+result);
    }
}
