package com.company;

enum Position {
    CLEANER, DRIVER, PROGRAMMER, TESTER, TEAMLEADER, PROJECTMANAGER, SENIORMANAGER, MANAGER
}

enum Category {
    FIRST, SECOND, THIRD
}

enum Workshift {
    DAY, NIGHT
}

abstract class Employee {
    int id;
    String name;
    double payment;
    Position position;
    Employee(int id, String name, Position position) {
        this.id = id;
        this.name = name;
        this.position = position;
    }
}

interface WorkTime {
    double calcPaymentWorkTime(double worktime, double salary);
}

interface Project {
    double budget = 1_000_000;
    double calcPaymentProject(Position position);
}

interface Heading {
    double calcPaymentHeading(int subordinates);
}

class Engineer extends Employee implements WorkTime, Project {
    Category category;
    double worktime;
    Engineer (int id, String name, Position position, Category category, double worktime) {
        super(id, name, position);
        this.category = category;
        this.worktime = worktime;
    }
    public double calcPaymentWorkTime(double worktime, double salary) {
        double sum;
        sum = worktime * salary;
        switch (category) {
            case FIRST:
                sum += ((sum * 30) / 100);
                break;
            case SECOND:
                sum += ((sum * 20) / 100);
                break;
            case THIRD:
                sum += ((sum * 10) / 100);
                break;
        }
        return sum;
    }

    public double calcPaymentProject(Position position) {
        double sum = 0;
        switch (position) {
            case PROGRAMMER:
                sum = (budget * 20) / 100;
                break;
            case TESTER:
                sum = (budget * 15) / 100;
                break;
            case TEAMLEADER:
                sum = (budget * 20) / 100;
                break;
        }
        return sum;
    }
}

class Programmer extends Engineer {
    static int count;
    final int SALARY = 300;
    Programmer (int id, String name, Position position, Category category, double worktime) {
        super(id, name, position, category, worktime);
        count++;
        payment = calcPaymentWorkTime(worktime, SALARY) + calcPaymentProject(position);
    }
}

class Tester extends Engineer {
    static int count;
    final int SALARY = 300;
    Tester (int id, String name, Position position, Category category, double worktime) {
        super(id, name, position, category, worktime);
        count++;
        payment = calcPaymentWorkTime(worktime, SALARY) + calcPaymentProject(position);
    }
}

class TeamLeader extends Programmer implements Heading {
    int subordinates;
    TeamLeader(int id, String name, Position position, Category category, double worktime) {
        super(id, name, position, Category.FIRST, worktime);
        subordinates = Programmer.count + Tester.count;
        payment = calcPaymentWorkTime(worktime, SALARY) + calcPaymentProject(position) + calcPaymentHeading(subordinates);
    }
    public double calcPaymentHeading(int subordinates) {
        double sum;
        sum = subordinates * 5000;
        return sum;
    }
}

class Personal extends Employee implements WorkTime {
    double worktime;
    Workshift workshift;
    Personal(int id, String name, Position position, Workshift workshift, double worktime) {
        super(id, name, position);
        this.workshift = workshift;
        this.worktime = worktime;
    }
    public double calcPaymentWorkTime(double worktime, double salary) {
        double sum;
        sum = worktime * salary;
        if (workshift == Workshift.NIGHT)
            sum *= 1.5;
        return sum;
    }
}

class Cleaner extends Personal {
    final int SALARY = 350;
    Cleaner(int id, String name, Position position, Workshift workshift, double worktime) {
        super(id, name, position, workshift, worktime);
        payment = calcPaymentWorkTime(worktime, SALARY);
    }
}

class Driver extends Personal {
    final int SALARY = 350;
    Driver(int id, String name, Position position, Workshift workshift, double worktime) {
        super(id, name, position, workshift, worktime);
        payment = calcPaymentWorkTime(worktime, SALARY);
    }
}

class Manager extends Employee implements Project {
    static int count;
    Manager(int id, String name, Position position) {
        super(id, name, position);
        count++;
        payment = calcPaymentProject(position) + 50000;
    }
    public double calcPaymentProject(Position position) {
        double sum = 0;
        switch (position) {
            case PROJECTMANAGER:
                sum = (budget * 7) / 100;
                break;
            case SENIORMANAGER:
                sum = (budget * 15) / 100;
                break;
            case MANAGER:
                sum = (budget * 3) / 100;
                break;
        }
        return sum;
    }
}

class ProjectManager extends Manager implements Heading {
    static int count;
    int subordinates;
    ProjectManager(int id, String name, Position position) {
        super(id, name, position);
        count++;
        subordinates = Manager.count;
        payment = calcPaymentProject(position) + calcPaymentHeading(subordinates);
    }
    public double calcPaymentHeading(int subordinates) {
        double sum;
        sum = subordinates * 15000;
        return sum;
    }
}

class SeniorManager extends ProjectManager {
    int subordinates;
    SeniorManager(int id, String name, Position position) {
        super(id, name, position);
        subordinates = ProjectManager.count;
        payment = calcPaymentProject(position) + calcPaymentHeading(subordinates);
    }
}

public class StaffDemo {

    public static void main(String[] args) {
        Programmer programmer_one = new Programmer(1, "Сверчков Изот Хрисанфович", Position.PROGRAMMER, Category.THIRD, 120);
        Programmer programmer_two = new Programmer(2, "Оффенберг Феклист Валентинович", Position.PROGRAMMER, Category.SECOND, 120);
        TeamLeader teamLeader = new TeamLeader(3, "Юрьевский Трофим Аврамович", Position.TEAMLEADER, Category.FIRST, 120);
        Tester tester = new Tester(4, "Собакин Уриил Братиславович", Position.TESTER, Category.FIRST, 120);
        Manager manager = new Manager(5, "Божич Тимур Филатович", Position.MANAGER);
        ProjectManager projectManager = new ProjectManager(6, "Полторацкий Никита Еремеевич", Position.PROJECTMANAGER);
        SeniorManager seniorManager = new SeniorManager(7, "Остроградский Патапий Изотович", Position.SENIORMANAGER);
        Driver driver = new Driver(8, "Бобрищев Ероним Егорович", Position.DRIVER, Workshift.DAY, 120);
        Cleaner cleaner_one = new Cleaner(9, "Бурдукова Лилия Ипполитовна", Position.CLEANER, Workshift.DAY, 120);
        Cleaner cleaner_two = new Cleaner(10, "Деменкова Агния Харламовна", Position.CLEANER, Workshift.NIGHT, 120);
        System.out.println(programmer_one.id + "    " + programmer_one.name + "          " + programmer_one.position + "     " + programmer_one.category + "      " + programmer_one.worktime + "     " + programmer_one.payment);
        System.out.println(programmer_two.id + "    " + programmer_two.name + "     " + programmer_two.position + "     " + programmer_two.category + "     " + programmer_two.worktime + "     " + programmer_two.payment);
        System.out.println(teamLeader.id + "    " + teamLeader.name + "         " + teamLeader.position + "     " + teamLeader.category + "      " + teamLeader.worktime + "     " + teamLeader.payment);
        System.out.println(tester.id + "    " + tester.name + "        " + tester.position + "         " + tester.category + "      " + tester.worktime + "     " + tester.payment);
        System.out.println(manager.id + "    " + manager.name + "              " + manager.position + "                             " + manager.payment);
        System.out.println(projectManager.id + "    " + projectManager.name + "       " + projectManager.position + "                      " + projectManager.payment);
        System.out.println(seniorManager.id + "    " + seniorManager.name + "     " + seniorManager.position + "                       " + seniorManager.payment);
        System.out.println(driver.id + "    " + driver.name + "           " + driver.position + "         " + driver.workshift + "        " + driver.worktime + "     " + driver.payment);
        System.out.println(cleaner_one.id + "    " + cleaner_one.name + "        " + cleaner_one.position + "        " + cleaner_one.workshift + "        " + cleaner_one.worktime + "     " + cleaner_one.payment);
        System.out.println(cleaner_two.id + "   " + cleaner_two.name + "         " + cleaner_two.position + "        " + cleaner_two.workshift + "      " + cleaner_two.worktime + "     " + cleaner_two.payment);

    }
}
