//package lab_1;

/**
 * Created by Юрец on 25.06.2017.
 */


public class lab_1_1 {


    public static void main(String[] args) {
        int max = 0;
		int someinteger = 0;
        for (int i = 1; i < 1000000; i++) {
            if (Conjecture(i) > max) {
                max = Conjecture(i);
				someinteger = i;
            }
        }

        System.out.println("Максимальная последовательность для данного диапазона:" + max+ " у числа:"+ someinteger);

       
    }

    private static int Conjecture(int integer) {
        int c = 0;
        do {
            if (integer % 2 == 0) {
                integer = integer / 2;
            } else {
                integer = integer * 3 + 1;
            }
            c++;
        } while (integer != 1);
        return c;

    }
}
