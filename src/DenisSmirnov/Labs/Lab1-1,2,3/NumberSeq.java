public class NumberSeq {

    //''1,2,4-7,18-21'' -> 1,2,4,5,6,7,18,19,20,21
    public static void main(String[] args) {
        String input = args[0];
        String expanded = expand(input);
        System.out.println(expanded);
    }
   
    public static String expand(String input) {
        String result = "";
        String[] split = input.split(",");
        for (String elem : split) {
            if (!elem.contains("-")) {
                if (!result.isEmpty()) {
                    result += ",";
                }
                result += elem;
            } else {
                String[] range = elem.split("-");
                int from = Integer.valueOf(range[0]);
                int to = Integer.valueOf(range[1]);
                for (int i = from; i <= to; i++) {
                    if (!result.isEmpty()) {
                        result += ",";
                    }
                    result += i;
                }
            }
        }
        return result;
    }

}
